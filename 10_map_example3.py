# /usr/bin/python3


def add(first, second):
    return first + second


def multiply(first, second):
    return first * second


def divide(first, second):
    return first / second


def modulo(first, second):
    return first % second


def run(funcs, values):
    data = []

    for data_set in values:
        anon_func = lambda func: func(data_set['first'], data_set['second'])
        value = list(map(anon_func, funcs))
        data.append(value)

    annon_func2 = lambda args: {'multiply': args[0], 'add': args[1], 'divide': args[2], 'modulo': args[3]}
    data2 = list(map(annon_func2, data))

    return data2


if __name__ == '__main__':
    funcs = [multiply, add, divide, modulo]
    values = [
        {'first': 1, 'second': 2},
        {'first': 2, 'second': 3},
        {'first': 3, 'second': 4},
        {'first': 4, 'second': 5},
        {'first': 5, 'second': 6},
        {'first': 6, 'second': 7}
    ]

    print(run(funcs, values))
