#/usr/bin/python3


def map_example(max):
    items = list(range(max))
    return list(map(lambda x: x**2, items))

if __name__ == '__main__':
    print(map_example(10))