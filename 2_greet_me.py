#!/usr/bin/python3


def greet_me(**kwargs):
    for key, value in kwargs.items():
        print("{}:{}".format(key, value))


if __name__== '__main__':
    greet_me(name='yasoob')