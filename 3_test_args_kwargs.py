#!/usr/bin/python3


def test_args_kwargs(arg1, arg2, arg3):
    print("arg1: {}".format(arg1))
    print("arg2: {}".format(arg2))
    print("arg3: {}".format(arg3))

if __name__ == '__main__':
    args = (1,2,3)
    kwargs = {'arg1': 1, 'arg3': 2, 'arg2': 3}
    test_args_kwargs(*args)
    test_args_kwargs(**kwargs)