#!/usr/bin/python3


def generator_function():
    for i in range(1000000):
        yield i


if __name__=='__main__':
    for item in generator_function():
        print(item)