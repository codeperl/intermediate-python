#/usr/bin/python3


def map_example2(max):
    items = tuple(range(max))
    return set(map(lambda x: x**2, items))

if __name__ == '__main__':
    print(map_example2(10))