#!/usr/bin/python3


def fibon(n):
    a = 0
    b = 1
    result = []

    for i in range(n):
        result.append(a)
        a, b = b, a + b

    return result


if __name__ == '__main__':
    print(fibon(100))